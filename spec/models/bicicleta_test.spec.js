var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Test Bicicletas', function(){
    var originalTimeout;
    beforeEach(function(done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        console.log(jasmine.DEFAULT_TIMEOUT_INTERVAL);
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('coneccion establecida');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    describe('Bicicleta.createInstance', () =>{
        it('crea una instancia de bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, 'Morado', 'urbana', [-34.5, -58.3]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Morado');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-58.3);
        });
    });

    describe('Bicicleta.allBicis', () =>{
        it('comienza vacia', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();                
            });
        });        
    });

    describe('Bicicleta.add', () =>{
        it('agrega solo una bicicleta', (done) =>{
            var aBici = new Bicicleta({code:1, color: 'verde', modelo:"urbana"});
            Bicicleta.add(aBici,function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();                
                });
            });            
        });        
    });

    describe('Bicicleta.findByCode', () =>{
        it('agrega solo una bicicleta', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                
                var aBici = new Bicicleta({code:1, color: 'verde', modelo:"urbana"});
                Bicicleta.add(aBici,function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color: 'roja', modelo:"urbana"});
                    Bicicleta.add(aBici2,function(err, newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();                
                        });
                    });
                });  
            });                          
        });        
    });
});
