var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

beforeEach(() =>{Bicicleta.allBicis = [];console.log("testeando"); });

describe('Bicicleta API', ()=>{
    describe('get bicicletas', ()=>{
        it('Status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana', [-34.5712424, -58.3861497]);
            Bicicleta.add(a);

            request.get(base_url, function (error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('post bicicletas /create', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id": 10, "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58}';
            
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("negro");
                done();
            });
        });
    });

    describe('delete bicicletas /delete', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id": 10, "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58}';
            
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("negro");
                request.delete({
                    headers: headers,
                    url: base_url + '/delete',
                    body: '{"id": 10}'
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(204);
                    done();
                });
            });
        });
    });
});
